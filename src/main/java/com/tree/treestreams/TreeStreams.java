package com.tree.treestreams;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.google.gson.Gson;
import com.tree.treestreams.stream.StreamManager;

@SpringBootApplication
public class TreeStreams extends SpringBootServletInitializer {

	private static StreamManager streamManager;

	public static void main(String[] args) {
		new Thread(() -> {
			try {
				while (true) {
					loadStreams();
					Thread.sleep(30000L);
				}
			} catch (Exception ex) {
			}
		}).start();

		SpringApplication.run(TreeStreams.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(TreeStreams.class);
	}

	public static void loadStreams() {
		File streams = new File("streams.json");
		if (!streams.exists()) {
			streamManager = new StreamManager();
			try {
				FileWriter fw = new FileWriter(streams);
				new Gson().toJson(streamManager, fw);
				fw.close();
			} catch (Exception ex) {
				System.err.println("Error: An error has occured loading streams!");
				System.exit(1);
			}
		} else {
			try {
				streamManager = new Gson().fromJson(new FileReader(streams), StreamManager.class);
			} catch (Exception ex) {
				System.err.println("Error: An error has occured loading streams!");
				System.exit(1);
			}
		}
	}

	public static StreamManager getStreamManager() {
		return streamManager;
	}

}
