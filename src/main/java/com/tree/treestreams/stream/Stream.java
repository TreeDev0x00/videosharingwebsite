package com.tree.treestreams.stream;

import java.util.UUID;

public class Stream {

	private String description;
	private StreamType type;
	private String id;
	private String m3u8;
	private String mimeType;
	private String bannerPath;
	private long creationTime;
	
	public Stream(String description, StreamType type, String m3u8, String mimeType, String bannerPath) {
		this.description = description;
		this.creationTime = System.currentTimeMillis();
		this.type = type;
		this.id = UUID.randomUUID().toString().replace("-", "");
		this.m3u8 = m3u8;
		this.mimeType = mimeType;
		this.bannerPath = bannerPath;
	}

	public String getDescription() {
		return description;
	}
	
	public long getCreationTime() {
		return creationTime;
	}
	
	public StreamType getStreamType() {
		return type;
	}
	
	public String getId() {
		return id;
	}

	public String getM3u8() {
		return m3u8;
	}

	public void setM3u8(String m3u8) {
		this.m3u8 = m3u8;
	}
	
	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}  

	public String getBannerPath() {
		return bannerPath;
	}

	public void setBannerPath(String bannerPath) {
		this.bannerPath = bannerPath;
	}
	
}
