package com.tree.treestreams.stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StreamManager {

	private Map<String, Stream> streams;
	
	public StreamManager() {
		this.streams = new HashMap<>();
	}
	
	public List<Stream> getStreamsByType(StreamType type) {
		List<Stream> result = new ArrayList<>();
		for (Stream str : streams.values()) {
			if (str.getStreamType() == type) {
				result.add(str);
			}
		}
		return result;
	}
	
	public void addStream(Stream stream) {
		streams.put(stream.getId(), stream);
	}
	
	public void removeStream(String streamId) {
		streams.remove(streamId);
	}
	
	public Collection<Stream> getStreams() {
		return streams.values();
	}
	
	public Set<String> getStreamIds() {
		return streams.keySet();
	}
	
}
