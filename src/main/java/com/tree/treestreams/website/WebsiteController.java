package com.tree.treestreams.website;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.tree.treestreams.TreeStreams;
import com.tree.treestreams.stream.Stream;
import com.tree.treestreams.stream.StreamType;

@Controller
public class WebsiteController {

	@GetMapping("/")
	public String index(Map<String, Object> model) {
		return "index";
	}
	
	@GetMapping("/disclaimer")
	public String disclaimer(Map<String, Object> model) {
		return "disclaimer";
	}
	
	/*@GetMapping("/football")
	public String football(Map<String, Object> model) {
		model.put("streams", getNFLStreams());
		List<Stream> streams = TreeStreams.getStreamManager().getStreamsByType(StreamType.NFL);
		if (streams.size() > 0) { model.put("latestStreamM3U8", streams.get(0).getM3u8()); }
		return "football";
	}*/
	
	@GetMapping("/football2")
	public String football2(Map<String, Object> model) {
		model.put("streams", getXFLStreams());
		List<Stream> streams = TreeStreams.getStreamManager().getStreamsByType(StreamType.XFL);
		if (streams.size() > 0) { model.put("latestStreamM3U8", streams.get(0).getM3u8()); }
		return "football2";
	}
	
	@GetMapping("/wrestling")
	public String wrestling(Map<String, Object> model) {
		model.put("streams", getWWEStreams());
		List<Stream> streams = TreeStreams.getStreamManager().getStreamsByType(StreamType.WWE);
		if (streams.size() > 0) { model.put("latestStreamM3U8", streams.get(0).getM3u8()); }
		return "wrestling";
	}
	
	private String getNFLStreams() {
		StringBuilder result = new StringBuilder();
		for (Stream str : TreeStreams.getStreamManager().getStreamsByType(StreamType.NFL)) {
			result.append(String.format("<div class=\"games\" style=\"background-image:url('%s');\" onclick=\"changeSource('%s', '%s');\"><h2>%s</h2></div>\n", str.getBannerPath(), str.getM3u8(), str.getMimeType(), str.getDescription()));
		}
		return result.toString();
	}
	
	private String getXFLStreams() {
		StringBuilder result = new StringBuilder();
		for (Stream str : TreeStreams.getStreamManager().getStreamsByType(StreamType.XFL)) {
			result.append(String.format("<div class=\"games\" style=\"background-image:url('%s');\" onclick=\"changeSource('%s', '%s');\"><h2>%s</h2></div>\n", str.getBannerPath(), str.getM3u8(), str.getMimeType(), str.getDescription()));
		}
		return result.toString();
	}
	
	private String getWWEStreams() {
		StringBuilder result = new StringBuilder();
		for (Stream str : TreeStreams.getStreamManager().getStreamsByType(StreamType.WWE)) {
			result.append(String.format("<div class=\"games\" style=\"background-image:url('%s');\" onclick=\"changeSource('%s', '%s');\"><h2>%s</h2></div>\n", str.getBannerPath(), str.getM3u8(), str.getMimeType(), str.getDescription()));
		}
		return result.toString();
	}

}
